import time
import allure

from selenium import webdriver
from selenium.webdriver.common.by import By
@allure.feature('搜索功能')
class TestCESHIREN():

    def setup_method(self, method):

        self.driver = webdriver.Chrome()
        self.vars = {}

    def teardown_method(self, method):
        self.driver.quit()

    @allure.title('测试过程')
    @allure.story('测试过程')
    def test_cESHIREN(self):
        with allure.step('打开浏览器并进入搜索引擎首页'):
          self.driver.get("https://ceshiren.com/")
        time.sleep(3)
        self.driver.set_window_size(1060, 816)
        with allure.step('定位搜索按钮'):
          self.driver.find_element(By.CSS_SELECTOR, ".d-icon-search").click()
          self.driver.find_element(By.ID, "search-term").click()
        with allure.step('输入关键词'):
          self.driver.find_element(By.ID, "search-term").send_keys("LHL")
        time.sleep(3)
        with allure.step('做断言'):
          assert self.driver.find_element(By.CSS_SELECTOR, ".item:nth-child(1) .name").text == "LHL"
        self.driver.find_element(By.CSS_SELECTOR, ".item:nth-child(1) .user-result").click()